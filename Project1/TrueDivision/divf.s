# divf.s
# floating-point division

# Student Name:	Juan Jose Ospina	
# Date: 11/10/2015

.text 
.globl fdiv 

# Preconditions:	
#   1st parameter (a0) single precision floating point Dividend
#   2nd parameter (a1) single precision floating point Divisor
# Postconditions:
#   result (v0) single precision floating point Quotient
# Divisor  /__Dividend___ 
# -------	Quotient
# Remainder

fdiv:	or $v0,$0,$0			# v0 = 0, the default result
	beq $a0,$0,return		# return if multiplicand is zero
	beq $a1,$0,return		# return if multiplier is zero
	
	# place mask for leftmost bit in t5
	li $t5,0x80000000		# t5 = 0x80000000
	
	# place sign of multiplicand in t0
	and $t0,$a0,$t5			# mask off exponent and significand

	# place sign of multiplier in t1
	and $t1,$a1,$t5			# mask off exponent and significand

	# place sign of product in t2
	xor $t2,$t0,$t1			# t2 = xor of signs

	# place exponent of multiplicand in t0
	sll $t0,$a0,1			# shift to remove sign bit
	srl $t0,$t0,24			# shift to remove significand bits
	addiu $t0,$t0,-127		# subract exponent bias

	# place exponent of multiplier in t1
	sll $t1,$a1,1			# shift to remove sign bit
	srl $t1,$t1,24			# shift to remove significand bits
	addiu $t1,$t1,-127		# subract exponent bias

	# place exponent of product in t3
	# ignore the possibility of overflow or underflow
	subu $t3,$t1,$t0		# t3 = substraction of exponents
	addi $t3,$t3,127		# add exponent bias
	
	# place significand of multiplicand in t0
	sll $t0,$a0,9			# shift to remove exponent
	li $t5,0x00800000		# restore implicit 1 to left of significand
	srl $t0,$t0,9
	or $t0,$t5,$t0
	# place significand of multiplier in t1
	sll $t1,$a1,9			# shift to remove exponent
	srl $t1,$t1,9
	or $t1,$t5,$t1			# restore implicit 1 to left of significand

	# place significand of product in t4
	# ignore rounding and overflow
	divu $t0,$t1			# multiply significands (unsigned)
	mfhi $t4			# t4 = high word of product
	#mfhi $t7			# --- ADDED to WORK
	# Normalization 
	srl $t6,$t4,15
	andi $t6,$t6,1
	beq $t6,$0,norm			# branch if already normalized
	
	#bge $t4,$0,norm
	
	srl $t4,$t4,1			# shift significand to normalize
	addi $t3,$t3,1			# adjust exponent
norm:	
	#sll $t4,$t4,16			# ---- ADDED - shift to remove implicit 1 better
	sll $t4,$t4,2			# shift to remove implicit 1
	
	# assemble product in v0
	sll $t3,$t3,23			# shift exponent into proper position
	srl $t4,$t4,9			# shift significand into proper position
	move $v0,$t2			# place sign in v0
	or $v0,$v0,$t3			# place exponent in v0
	or $v0,$v0,$t4			# place significand in v0

return:	jr $ra				# return

