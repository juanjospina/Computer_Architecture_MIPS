# floatopt.s
# floating-point multiplication OPTIMIZED

# Student Name:	Juan Jose Ospina	
# Date: 11/10/2015

.text 
.globl fmultopt 

# Preconditions:	
#   1st parameter (a0) single precision floating point multiplicand
#   2nd parameter (a1) single precision floating point multiplier
# Postconditions:
#   result (v0) single precision floating point product

fmultopt:
	or $v0,$0,$0			# v0 = 0, the default result
	beq $a0,$0,return		# return if multiplicand is zero
	beq $a1,$0,return		# return if multiplier is zero
	
	# place sign of product in t2
	li $t5,0x80000000
	xor $t2,$a0,$a1			# OPTIMIZED t2 = xor of signs
	and $t2,$t2,$t5
#--------------OPTIMIZED-------------------------------------
	li $t5, 0x7f800000 		# Exponent mask
	and $t7, $a0, $t5              # isolate exponents
        and $t8, $a1, $t5
        
        srl $t7, $t7, 23               # shift exponent to the right
        srl $t8, $t8, 23
        
        addi $t7, $t7, -127             # subtract bias
        addi $t8, $t8, -127
        
        addu $t3,$t7,$t8		# t3 = sum of exponents
	addi $t3,$t3,127		# add exponent bias
	
	# t3 Has EXPONENT
	
	 li $t5 0x007fffff
         and $t0, $a0, $t5               # isolation of mantissas
         and $t1, $a1, $t5
         
         li $t5 0x00800000
         add $t0, $t0, $t5               # include leading 1
         add $t1, $t1, $t5

	# t0 has mantissa 1
	# t1 has mantissa 2
	
#-------------------------------------------------
	# place significand of product in t4
	# ignore rounding and overflow
	multu $t0,$t1			# multiply significands (unsigned)
	mfhi $t4			# t4 = high word of product
	# Normalization 
	srl $t6,$t4,15
	andi $t6,$t6,1
	beq $t6,$0,norm			# branch if already normalized 
	
	srl $t4,$t4,1			# shift significand to normalize
	addi $t3,$t3,1			# adjust exponent
norm:	
	sll $t4,$t4,18			# shift to remove implicit 1 process

	# assemble product in v0
	sll $t3,$t3,23			# shift exponent into proper position
	srl $t4,$t4,9			# shift significand into proper position
	move $v0,$t2			# place sign in v0
	or $v0,$v0,$t3			# place exponent in v0
	or $v0,$v0,$t4			# place significand in v0

return:	jr $ra				# return

