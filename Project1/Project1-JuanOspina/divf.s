# divf.s
# floating-point division

# Student Name:	Juan Jose Ospina	
# Date: 11/10/2015

.text 
.globl fdiv 

# Preconditions:	
#   1st parameter (a0) single precision floating point Dividend
#   2nd parameter (a1) single precision floating point Divisor
# Postconditions:
#   result (v0) single precision floating point Quotient
# Divisor  /__Dividend___ 
# -------	Quotient
# Remainder

fdiv:	or $v0,$0,$0			# v0 = 0, the default result
	beq $a0,$0,return		# return if Divisor is zero
	beq $a1,$0,return		# return if Dividend is zero
	
	# place mask for leftmost bit in t5
	li $t5,0x80000000		# t5 = 0x80000000
	
	# place sign of multiplicand in t0
	and $t0,$a0,$t5			# mask off exponent and significand

	# place sign of multiplier in t1
	and $t1,$a1,$t5			# mask off exponent and significand

	# place sign of product in t2
	xor $t2,$t0,$t1			# t2 = xor of signs

	# place exponent of multiplicand in t0
	sll $t0,$a0,1			# shift to remove sign bit
	srl $t0,$t0,24			# shift to remove significand bits
	addiu $t0,$t0,-127		# subract exponent bias

	# place exponent of multiplier in t1
	sll $t1,$a1,1			# shift to remove sign bit
	srl $t1,$t1,24			# shift to remove significand bits
	addiu $t1,$t1,-127		# subract exponent bias

	# place exponent of product in t3
	# ignore the possibility of overflow or underflow
	subu $t3,$t0,$t1		# t3 = substraction of exponents
	addi $t3,$t3,127		# add exponent bias
	
	# place significand of multiplicand in t0
	sll $t0,$a0,9			# shift to remove exponent
	li $t5,0x00800000		# restore implicit 1 to left of significand
	srl $t0,$t0,9
	or $t0,$t5,$t0
	# place significand of multiplier in t1
	sll $t1,$a1,9			# shift to remove exponent
	srl $t1,$t1,9
	or $t1,$t5,$t1			# restore implicit 1 to left of significand
	
	# Shifts to perform correct division
	# Both numbers are shifted until find a 1
shift1:
	or $t9,$0,$0			# Clears t9
	srl $t0,$t0,1			# Shift right until a 1 appears
	andi $t9,$t0,1			# Check LSB
	beq $t9,$0,shift1		# Branch if a 1 is not found on LSB
	
shift2:
	or $t9,$0,$0			# Clears t9
	srl $t1,$t1,1			# Shift right until a 1 appears
	andi $t9,$t1,1			# Check LSB
	beq $t9,$0,shift2		# Branch if a 1 is not found on LSB
	
	
	divu $t0,$t1			# divide significands (unsigned)
	mflo $t4			# t4 = quotient of division
	mfhi $t7			# t7 = remainde of division
		
	bge $t0,$t1,norm		# Branch if normalization not required.

	#srl $t4,$t4,1			# shift significand to normalize
	addi $t3,$t3,-1			# adjust exponent
norm:	
	li $t5, 0x80000000
	or $t6,$0,$0		#Counter for 32 iterations

	# Loop used to remove implicit 1 added.
shift3:
	or $t9,$0,$0
	sll $t4,$t4,1
	and $t9,$t4,$t5			# AND used to check the MSB 
	addiu $t6,$t6,1			# Counter + 1
	beq $t6,32,out			# Break Loop if Not 1 is found 
	beq $t9,$0,shift3		# Branch if sitll not got the 1
	sll $t4,$t4,1 		        # Removes implicit 1
	
out:
	# assemble product in v0
	sll $t3,$t3,23			# shift exponent into proper position
	srl $t4,$t4,9			# shift significand into proper position
	move $v0,$t2			# place sign in v0
	or $v0,$v0,$t3			# place exponent in v0
	or $v0,$v0,$t4			# place significand in v0

return:	jr $ra				# return
